package com.pot.model;

import org.springframework.stereotype.Component;

/**
 * Created by priv on 02.02.16.
 */
@Component("ps4")
public class PS4 implements Console {

    @Override
    public String getName() {
        return "PS4";
    }

}
