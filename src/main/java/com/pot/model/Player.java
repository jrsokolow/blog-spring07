package com.pot.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by priv on 13.01.16.
 */
@Component
public class Player {

    @Autowired
    @Qualifier("x1")
    private Console console;

    @Override
    public String toString() {
        return "This player plays on " + this.console.getName();
    }

}
